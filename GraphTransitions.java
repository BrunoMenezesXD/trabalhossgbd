import java.util.ArrayList;

public class GraphTransitions
{
    // Vértices do grafo de transições. Cada vértice representa um possível estado.
    
    private ArrayList<Transaction> TR_Iniciada;
    private ArrayList<Transaction> Ativa;
    private ArrayList<Transaction> Processo_Cancelamento;
    private ArrayList<Transaction> Processo_Efetivacao;
    private ArrayList<Transaction> Efetivada;
    private ArrayList<Transaction> TR_Finalizada;
    
    // Arestas. São geradas à medida que ocorre um evento pela primeira vez.
    // TR_Inicializada = 0. Ativa = 1. Processo_Cancelamento = 2. Processo_Efetivacao = 3.
    // Efetivada = 4. TR_Finalizada = 5.
    
    private ArrayList<Integer> vertex0;
    private ArrayList<Integer> vertex1;
    private ArrayList<Integer> vertex2;
    private ArrayList<Integer> vertex3;
    private ArrayList<Integer> vertex4;
    private ArrayList<Integer> vertex5;
    
    // Lista com todas as transações no grafo.
    
    ArrayList<Transaction> transactionsOnGraph = null;
    
    // Métodos.
    
    private void inicializarArestas()
    {
        vertex0 = new ArrayList();
        vertex1 = new ArrayList();
        vertex2 = new ArrayList();
        vertex3 = new ArrayList();
        vertex4 = new ArrayList();
        vertex5 = new ArrayList();
    }
    
    public boolean grafoInicializado()
    {
        return (transactionsOnGraph != null);
    }
    
    public boolean inicializarGrafo()
    {
        if(transactionsOnGraph != null)
            return false;
        else
        {
            transactionsOnGraph = new ArrayList();
            TR_Iniciada = new ArrayList();
            Ativa = new ArrayList();
            Processo_Cancelamento = new ArrayList();
            Processo_Efetivacao = new ArrayList();
            Efetivada = new ArrayList();
            TR_Finalizada = new ArrayList();
            this.inicializarArestas();
        }
        
        return true;
    }
    
    public void percorrerEstados()
    {

        int cont = 0;
        for (Transaction tn: transactionsOnGraph)
        {
            System.out.println("Estado da transação "+cont+": "+tn.getEstado()+".");
            cont++;
        }
        
    }
    
    public boolean verificarTransacao(int t) {
        boolean validade = false;
        if(t >= 0 && t < transactionsOnGraph.size() ) {
            
        if(transactionsOnGraph.contains( transactionsOnGraph.get(t))) {
            validade = true;
        
        }
       }
        
        return validade;
    }
    
    private void adicionarAresta(String estadoCorrente, String estado)
    {
        if(estadoCorrente.equals("TR_Inicalizada") && estado.equals("Ativa"))
        {
            vertex0.add(1);
        }
        if(estadoCorrente.equals("Ativa") && estado.equals("Ativa"))
        {
            vertex1.add(1);
        }
        if(estadoCorrente.equals("Ativa") && estado.equals("Processo_Efetivacao"))
        {
            vertex1.add(3);
        }
        if((estadoCorrente.equals("Ativa") || estadoCorrente.equals("Processo_Efetivacao")) && estado.equals("Processo_Cancelamento"))
        {
            if(estadoCorrente.equals("Ativa"))
                vertex1.add(2);
            if(estadoCorrente.equals("Processo_Efetivacao"))
                vertex3.add(2);
        }
        if(estadoCorrente.equals("Processo_Efetivacao") && estado.equals("Efetivada"))
        {
            vertex3.add(4);
        }
        if((estadoCorrente.equals("Processo_Cancelamento") || estadoCorrente.equals("Efetivada")) && estado.equals("TR_Finalizada"))
        {
            if(estadoCorrente.equals("Processo_Cancelamento"))
                vertex2.add(5);
            if(estadoCorrente.equals("Efetivada"))
                vertex4.add(5);
        }
    }
    
    public boolean mudarEstado(Transaction t, String estado)
    {
        String estadoCorrente = t.getEstado();
        if(estadoCorrente != null)
            this.adicionarAresta(estadoCorrente, estado);
        
        t.setEstado(estado);
        return true;
    }
    
    public boolean eventoBegin(Transaction t)
    {
        transactionsOnGraph.add(t);
        TR_Iniciada.add(t);
        return this.mudarEstado(t, "TR_Iniciada");
    }
    
    public boolean eventoReadWrite(int numTrans)
    {
        Transaction t = transactionsOnGraph.get(numTrans);
        if(TR_Iniciada.contains(t))
        {
            Ativa.add(t);
            TR_Iniciada.remove(t);
            return this.mudarEstado(t, "Ativa");
        }
        
        if(Ativa.contains(t))
        {
            return true;
        }
        
        return false;
    }
    
    public boolean eventoTerminate(int numTrans)
    {
        Transaction t = transactionsOnGraph.get(numTrans);
        if(Ativa.contains(t))
        {
            Processo_Efetivacao.add(t);
            Ativa.remove(t);
            return this.mudarEstado(t, "Processo_Efetivacao");
        }
        else
            return false;
    }
    
    public boolean eventoRollback(int numTrans)
    {
        Transaction t = transactionsOnGraph.get(numTrans);
        if(!Ativa.contains(t) && !Processo_Efetivacao.contains(t))
            return false;
        
        if(Ativa.contains(t))
        {
            Ativa.remove(t);
        }
        
        if(Processo_Efetivacao.contains(t))
        {
            Processo_Efetivacao.remove(t);
        }
        
        Processo_Cancelamento.add(t);
        return this.mudarEstado(t, "Processo_Cancelamento");
    }
    
    public boolean eventoCommit(int numTrans)
    {
        Transaction t = transactionsOnGraph.get(numTrans);
        if(Processo_Efetivacao.contains(t))
        {
            Efetivada.add(t);
            Processo_Efetivacao.remove(t);
            return this.mudarEstado(t, "Efetivada");
        }
        else
            return false;
    }
    
    public boolean eventoFinisth(int numTrans)
    {
        Transaction t = transactionsOnGraph.get(numTrans);
        if(!Processo_Cancelamento.contains(t) && !Efetivada.contains(t))
            return false;
        
        if(Processo_Cancelamento.contains(t))
        {
            Processo_Cancelamento.remove(t);
        }
        
        if(Efetivada.remove(t))
        {
            Efetivada.remove(t);
        }
        
        TR_Finalizada.add(t);
        return this.mudarEstado(t, "TR_Finalizada");
    }
}