import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

// Não mudar a separação por espaços no criarIndices.

public class CriarArquivos 
{
    public static String criarRegistros() throws IOException
    {
        FileWriter reg = new FileWriter("registrosDados.txt");
        PrintWriter gravarReg = new PrintWriter(reg);
        
        gravarReg.printf("V1    DonLaurindo Merlot      2011    Brasil%n");
		gravarReg.printf("V2	Panizzon    Chardonnay  1900    EUA%n");
		gravarReg.printf("V3	DonLaurindo Merlot      1999    Italia%n");
		gravarReg.printf("V4	DonLaurindo Tempranillo 1998    Japao%n");
		gravarReg.printf("V5	SaoBraz     Comum       2014    Brasil%n");
		gravarReg.printf("V6	Tree        Malbec      1708    Inglaterra%n");
		gravarReg.printf("V7	DonLaurindo Merlot      2002    Franca%n");
		gravarReg.printf("V8	Ibravin     Cabernet	2010	Canada%n");
		gravarReg.printf("V9	ChateauLafitte  Merlot  2008	Portugal%n");
		gravarReg.printf("V10	SaoBraz     Merlot	1997	Franca%n");
        reg.close();
        
        return "registrosDados.txt";
    }
    
    public static String criarIndices() throws IOException
    {
        FileWriter ind = new FileWriter("index.txt");
        PrintWriter gravarReg = new PrintWriter(ind);
        
        gravarReg.printf("1999 2 3 Eind 1%n"); // 1.
        gravarReg.printf("1998 5 8 Eind 2%n"); // 2.
		gravarReg.printf("2002 9 10 Eind 3%n"); // 3.
		gravarReg.printf("2010 10 12 Eind 3%n"); // 4.
		gravarReg.printf("1708 6 0 Edad 4%n"); // 5.
		gravarReg.printf("1900 2 0 Edad 4%n"); // 6.
		gravarReg.printf("1997 10 0 Edad 4%n"); // 7.
		gravarReg.printf("1998 4 0 Edad 5%n"); // 8.
		gravarReg.printf("1999 3 0 Edad 6%n"); // 9.
		gravarReg.printf("2002 7 0 Edad 7%n"); // 10. 
		gravarReg.printf("2008 9 0 Edad 7%n"); // 11.
		gravarReg.printf("2010 8 0 Edad 8%n"); // 12.
		gravarReg.printf("2011 1 0 Edad 8%n"); // 13.
		gravarReg.printf("2014 5 0 Edad 8%n"); // 14.

        
        ind.close();
        
        return "index.txt";
    }
    
    public static void deletarArquivo(String nomeArquivo)
    {
        File arq = new File(nomeArquivo);
        arq.delete();
    }
}
