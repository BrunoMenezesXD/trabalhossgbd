import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class LerArquivos 
{
    public static String lerLinha(String nomeArquivo, int numLinha)
    {
        try
        {
            int cont = 0;
            String linha = null;
            FileReader arq = new FileReader(nomeArquivo);
            BufferedReader lerArq = new BufferedReader(arq);
            
            while(cont < numLinha)
            {
                linha = lerArq.readLine();
                if(linha == null)
                    return null;
                cont++;
            }
            
            return linha;
        }
        catch (IOException e)
        {
            System.err.printf("Erro na abertura do arquivo: %s.\n", e.getMessage());
        }
        
        return null;
    }
    
    // Descobre a chave de uma entrada de índice ou dados.
    
    public static int descobrirChave(String entrada)
    {
        String[] valoresEntrada = entrada.split(" ");
        
        return Integer.parseInt(valoresEntrada[0]);
    }
    
    // Descobre o rid de uma entrada de índice ou dados conforme definida no enunciado do trabalho.
    
    public static String descobrirRidEsquerda(String entrada)
    {
        String[] valoresEntrada = entrada.split(" ");
        
        return valoresEntrada[1];
    }
    
    public static String descobrirRidDireita(String entrada)
    {
        String[] valoresEntrada = entrada.split(" ");
        
        return valoresEntrada[2];
    }
    
    // Descobre o tipo (Eind ou Edad) de uma entrada de índice ou dados conforme definida no enunciado do trabalho.
    
    public static String descobrirTipo(String entrada)
    {
        String[] valoresEntrada = entrada.split(" ");
        
        return valoresEntrada[3];
    }
    
    // Descobre o identificador de uma entrada. O idenficiador diz quais entradas pertecem ao mesmo nó.
    
    public static int descobrirId(String entrada)
    {
        String[] valoresEntrada = entrada.split(" ");
        
        return Integer.parseInt(valoresEntrada[4]);
    }
}