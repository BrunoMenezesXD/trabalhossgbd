public class ArvoreBPlus
{
    public final int ordem = 2;
    private int chaves[] = new int[2 * ordem];
    private String ponteiros[ ] = new String[2 * ordem + 1];
    private boolean folha = false;
    private int preenchido; // Indica quantos elementos o nó possui.
    
    public int getChave(int pos)
    {
        return chaves[pos];
    }
    
    public String getPonteiro(int pos)
    {
        return ponteiros[pos];
    }
    
    public int getOrdem()
    {
        return ordem;
    }
     
    public int getMaxOcc()
    {
        return preenchido;
    }
    
    public boolean isFolha()
    {
        return folha;
    }
    
    // No caso de um nó ser uma entrada de dados, o ponteiro para o registro de dados deve ficar à esquerda.
    // Entradas com o mesmo identificador devem ficar no mesmo nó.
    
    public void preencherNode(String nomeArq, int pos)
    {
        int i, idNode; // A variável id salva o identificador dos elementos deste nó.
        preenchido = 0;
        String linhaCorrente = LerArquivos.lerLinha(nomeArq, pos);
        idNode = LerArquivos.descobrirId(linhaCorrente);
        
        for(i = 0; i < 2 * ordem; i++)
        {
            linhaCorrente = LerArquivos.lerLinha(nomeArq, pos);
            
            if(linhaCorrente == null)
                break;
            if(LerArquivos.descobrirId(linhaCorrente) != idNode)
                break;
            
            chaves[i] = LerArquivos.descobrirChave(linhaCorrente);
            ponteiros[i] = LerArquivos.descobrirRidEsquerda(linhaCorrente);
            ponteiros[i + 1] = LerArquivos.descobrirRidDireita(linhaCorrente);
            preenchido++;
            
            if(LerArquivos.descobrirTipo(linhaCorrente).equals("Edad"))
                folha = true;
            
            pos++;
        }
    }
    
    public void percorrerNode()
    {
        for(int i = 0; i < preenchido; i++)
        {
            System.out.println(chaves[i]);
            System.out.println("Ponteiro esquerda: "+ponteiros[i]);
            System.out.println("Ponteiro direita: "+ponteiros[i + 1]);
        }
    }
}
