public class Busca
{
    public static String encontrarRegistro(ArvoreBPlus raiz, int chave, String nomeIndex, String nomeRegistros)
    {
        ArvoreBPlus aux = Busca.buscar(raiz, chave, nomeIndex);
        String linha = null;
        
        for(int i = 0; i < aux.getMaxOcc(); i++)
        {
            if(aux.getChave(i) == chave)
            {
                linha = LerArquivos.lerLinha(nomeRegistros, Integer.parseInt(aux.getPonteiro(i)));
            }
        }
        
        return linha;
    }
    
    public static ArvoreBPlus buscar(ArvoreBPlus no, int chave, String nomeIndex)
    {   
        if(no.isFolha())
            return no;
        
        ArvoreBPlus aux = new ArvoreBPlus();
        
        if(chave < no.getChave(0))
        {
            aux.preencherNode(nomeIndex, Integer.parseInt(no.getPonteiro(0)));
            return buscar(aux, chave, nomeIndex);
        }
   
        if(chave >= no.getChave(no.getMaxOcc() - 1)) 
        {
            aux.preencherNode(nomeIndex, Integer.parseInt(no.getPonteiro(no.getMaxOcc())));
            return buscar(aux, chave, nomeIndex);
        }
        
       int i = 0;
       for(i = 0; i < no.getMaxOcc(); i++)
        {
            if(no.getChave(i) <= chave && chave < no.getChave(i + 1))
                break;
        }
        
        aux.preencherNode(nomeIndex, Integer.parseInt(no.getPonteiro(i + 1)));
        
        return buscar(aux, chave, nomeIndex);
    }
}