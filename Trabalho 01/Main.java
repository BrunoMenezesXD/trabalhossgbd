import java.io.IOException;
import java.util.Scanner;
import java.io.File;

public class Main
{
    public static void main (String[] args) throws IOException
    {
        String arqRegistros = CriarArquivos.criarRegistros();
        String arqIndex = CriarArquivos.criarIndices();
        ArvoreBPlus raiz = new ArvoreBPlus();
        Scanner teclado = new Scanner(System.in);
        String registro;
        int chave = 0;
        
        raiz.preencherNode(arqIndex, 1);
        
        System.out.println("Para encerrar o programa, entre com o valor -1.");
        
        while(chave != -1)
        {
            System.out.println("Entre com a chave de busca. (Ano da colheira do vinho)");
            chave = teclado.nextInt();
         
         	if(chave != -1)
         	{       
            	registro = Busca.encontrarRegistro(raiz, chave, arqIndex, arqRegistros);
            	if(registro == null)
                	System.out.println("Registro com a chave entrada não encontrado.");
            	else
                	System.out.println(registro);
        	}
        }
        
        File f1 = new File("registrosDados.txt");
        File f2 = new File("index.txt");
        
        f1.delete();
        f2.delete();
        
    }
}
