import java.util.Arrays;
import java.util.Scanner;

public class User 
{   
    public static void mensagemSucesso()
    {
        System.out.println("Estado mudado com sucesso.");
    }
    
    public static void mensagemErro()
    {
        System.out.println("Erro ao mudar estado.");
    }
    
    public static void mensagemObterTransacao()
    {
        System.out.println("Entre com a transação, identificada pelo número (0, ..., n - 1), que deseja modificar o estado.");
    }
    
    public static void mensagemGrafoInicializado()
    {
        System.out.println("Grafo sem transações. Insira alguma transação no grafo.");
    }

    public static void mensagemMenu()
    {
            System.out.println("\nEscolha um evento.");
            System.out.println("-TR_Begin.");
            System.out.println("-Read ou Write.");
            System.out.println("-TR_Terminate.");
            System.out.println("-TR_Rollback.");
            System.out.println("-TR_Commit.");
            System.out.println("-TR_Finish.");
            System.out.println("-Sair."); 
    }
    
    public static void menu2() {
        System.out.println("Entre com os comandos: ");
    }
    
    public static void menu3() {
        System.out.println("Entre novamente com os comandos: ");
    }
    
    public static String menuErro() {
        Scanner hist = new Scanner(System.in);
        System.out.println("ENTRADA INCORRETA!!");
        menu3();
        return hist.nextLine();
    }
    
    public static void menuTransacoes()
    {
        GraphTransitions grafo = new GraphTransitions();
        Transaction t = new Transaction();
        Scanner teclado = new Scanner(System.in);
        int contTrans = 0,  numTrans = 0;
        String controle = " ";
        
        boolean resposta = false;
        
        int rr = 0;
        
        do {
           
            if(rr != 2)  
              mensagemMenu();
           rr  = 0; 
            controle = teclado.nextLine();
            controle = controle.toUpperCase();
        
            
            if(controle.equals("TR_BEGIN"))
            {
                if(!grafo.grafoInicializado())
                {
                    resposta = grafo.inicializarGrafo();
                    if(resposta == true)
                        System.out.println("\nGrafo inicializado com sucesso.");
                    else
                        System.out.println("\nFalha ao inicializar o grafo.");
                }
                
                resposta = grafo.eventoBegin(new Transaction());
                if(resposta)
                {
                    System.out.println("Transação iniciada com sucesso.\n");
                    contTrans++;
                    System.out.println("-----------------------------------------------------------\n");
                    grafo.percorrerEstados();
                    System.out.println("\n-----------------------------------------------------------\n");
                }
                else
                {
                    System.out.println("Falha ao inicializar transação.");
                }
            } else {

            if(controle.equals("READ") || controle.equals("WRITE") || controle.equals("TR_TERMINATE") || controle.equals("TR_ROLLBACK") || controle.equals("TR_COMMIT") || controle.equals("TR_FINISH")) 
          
            {
                if(!grafo.grafoInicializado())
                {
                    User.mensagemGrafoInicializado();
                    continue;
                }
                
                User.mensagemObterTransacao();
                numTrans = teclado.nextInt();

                if(numTrans >= 0 && numTrans < contTrans)
                {
                    rr = 2;
                    if(controle.equals("READ") || controle.equals("WRITE"))
                        resposta = grafo.eventoReadWrite(numTrans);
                    if(controle.equals("TR_TERMINATE"))
                         resposta = grafo.eventoTerminate(numTrans);
                    if(controle.equals("TR_ROLLBACK"))
                        resposta = grafo.eventoRollback(numTrans);
                    if(controle.equals("TR_COMMIT"))
                        resposta = grafo.eventoCommit(numTrans);
                    if(controle.equals("TR_FINISH"))
                        resposta = grafo.eventoFinisth(numTrans);
                   
                    if(resposta)
                    {
                        User.mensagemSucesso();
                    }
                    else
                    {
                        User.mensagemErro();
                    }
                    System.out.println("-----------------------------------------------------------\n");
                    grafo.percorrerEstados();
                    System.out.println("\n-----------------------------------------------------------\n");
               }
                else 
                {
                    rr = 2;
                    System.out.println("Transação inválida.");
                }
            }
        }
         
               
     }   while(!controle.equals("SAIR"));
     
    }
    
    
    public static void menuHistorico() {
        
        GraphTransitions grafo = new GraphTransitions();
        Transaction t = new Transaction();
        
        //ENTRADA DE DADOS
        Scanner historia = new Scanner(System.in);
        String texto = new String();
        String c ; // string para verificar o case
        String string; // string para receber o texto dentro do case
        int i = 0, numTran = 0; 
        
        //teste
         grafo.inicializarGrafo();
         grafo.eventoBegin(new Transaction());
        // grafo.eventoBegin(new Transaction());
         grafo.eventoReadWrite(0);
        // grafo.eventoReadWrite(1);
       
        
        menu2();
        texto = historia.nextLine();
        
        //System.out.println(texto.length());
        
        //VERIFICACAO DOS DADOS
        
        while(i < texto.length()) {
          
           c  = texto.substring(i, i+1);
           System.out.println(c);
           switch(c) {
               
               case "B":
                   //1ª
                   string = texto.substring(i,i+5);
                   System.out.println(string);
                   
                   //2ª
                   numTran = Integer.parseInt(texto.substring(i+3,i+4));
                   if(grafo.verificarTransacao(numTran)) {
                       System.out.println("Transacao existe");
                   } else {
                       System.out.println("Não existe transacao");
                       texto = menuErro();
                       i = 0;
                       break;
                   }
                   
                   i = i + 5;
                   break;
                   
               case "r":
                   string = texto.substring(i, i+5);
                   System.out.println(string);
                   i = i + 5;
                   break;
               case "w":
                   string = texto.substring(i, i+5);
                   System.out.println(string);
                   i = i + 5;
               case "C":
                   string = texto.substring(i, i+4);
                   System.out.println(string);
                   i = i + 4;
               default:
                   texto = menuErro();
                   i = 0;
                   break;
           }
            
         
                       
            
            
        }
         
        
    }
}
