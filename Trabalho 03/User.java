import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class User 
{   
    public static void mensagemSucesso()
    {
        System.out.println("Estado mudado com sucesso.");
    }
    
    public static void mensagemErro()
    {
        System.out.println("Erro ao mudar estado.");
    }
    
    public static void mensagemObterTransacao()
    {
        System.out.println("Entre com a transação, identificada pelo número (0, ..., n - 1), que deseja modificar o estado.");
    }
    
    public static void mensagemGrafoInicializado()
    {
        System.out.println("Grafo sem transações. Insira alguma transação no grafo.");
    }

    public static void mensagemMenu()
    {
            System.out.println("\nEscolha um evento.");
            System.out.println("-TR_Begin.");
            System.out.println("-Read ou Write.");
            System.out.println("-TR_Terminate.");
            System.out.println("-TR_Rollback.");
            System.out.println("-TR_Commit.");
            System.out.println("-TR_Finish.");
            System.out.println("-Sair."); 
    }
    
    public static void menu2() {
        System.out.println("Entre com os comandos: ");
    }
    
    public static void menu3() {
        System.out.println("Entre novamente com os comandos: ");
    }
    
    public static String menuErro() {
        Scanner hist = new Scanner(System.in);
        System.out.println("ENTRADA INCORRETA!!");
        menu3();
        return hist.nextLine();
    }
    
    public static void menuTransacoes()
    {
        Tr_Manager grafo = new Tr_Manager();
        Transaction t = new Transaction();
        Scanner teclado = new Scanner(System.in);
        int contTrans = 0,  numTrans = 0;
        String controle = " ";
        
        boolean resposta = false;
        
        int rr = 0;
        
        do {
           
            if(rr != 2)  
              mensagemMenu();
           rr  = 0; 
            controle = teclado.nextLine();
            controle = controle.toUpperCase();
        
            
            if(controle.equals("TR_BEGIN"))
            {
                if(!grafo.grafoInicializado())
                {
                    resposta = grafo.inicializarGrafo();
                    if(resposta == true)
                        System.out.println("\nGrafo inicializado com sucesso.");
                    else
                        System.out.println("\nFalha ao inicializar o grafo.");
                }
                
                resposta = grafo.eventoBegin(new Transaction());
                if(resposta)
                {
                    System.out.println("Transação iniciada com sucesso.\n");
                    contTrans++;
                    System.out.println("-----------------------------------------------------------\n");
                    grafo.percorrerEstados();
                    System.out.println("\n-----------------------------------------------------------\n");
                }
                else
                {
                    System.out.println("Falha ao inicializar transação.");
                }
            } else {

            if(controle.equals("READ") || controle.equals("WRITE") || controle.equals("TR_TERMINATE") || controle.equals("TR_ROLLBACK") || controle.equals("TR_COMMIT") || controle.equals("TR_FINISH")) 
          
            {
                if(!grafo.grafoInicializado())
                {
                    User.mensagemGrafoInicializado();
                    continue;
                }
                
                User.mensagemObterTransacao();
                numTrans = teclado.nextInt();

                if(numTrans >= 0 && numTrans < contTrans)
                {
                    rr = 2;
                    if(controle.equals("READ") || controle.equals("WRITE"))
                        resposta = grafo.eventoReadWrite(numTrans);
                    if(controle.equals("TR_TERMINATE"))
                         resposta = grafo.eventoTerminate(numTrans);
                    if(controle.equals("TR_ROLLBACK"))
                        resposta = grafo.eventoRollback(numTrans);
                    if(controle.equals("TR_COMMIT"))
                        resposta = grafo.eventoCommit(numTrans);
                    if(controle.equals("TR_FINISH"))
                        resposta = grafo.eventoFinisth(numTrans);
                   
                    if(resposta)
                    {
                        User.mensagemSucesso();
                    }
                    else
                    {
                        User.mensagemErro();
                    }
                    System.out.println("-----------------------------------------------------------\n");
                    grafo.percorrerEstados();
                    System.out.println("\n-----------------------------------------------------------\n");
               }
                else 
                {
                    rr = 2;
                    System.out.println("Transação inválida.");
                }
            }
        }
         
               
     }   while(!controle.equals("SAIR"));
     
    }
    
    
    public static void tratador(String texto, Tr_Manager grafo) {
         String c ; // string para verificar o case
         String string; // string para receber o texto dentro do case
         ArrayList<Integer> num = new ArrayList<>();
         Lock_Manager lm = new Lock_Manager(grafo);
         ItemDados D;
         Map<String, ItemDados> dados = new HashMap();
         //retornarbloqueio
         
         int cont = 0;
              
         dados.put("x",new ItemDados("x", 0)); 
         dados.put("y",new ItemDados("y", 1)); 
         dados.put("z",new ItemDados("z", 2)); 
         
         lm.criarFilaItem(0);
         lm.criarFilaItem(1);
         lm.criarFilaItem(2);
         //lm.criarFilaItem(2);
        
         
         int i = 0, numTran = 0; 
         while(i < texto.length()) {
          
           c  = texto.substring(i, i+1);
           System.out.println(c);
           switch(c) {
               
               case "B":
                   //1ª
                   string = texto.substring(i,i+5);
                   System.out.println(string);
                   
                   //2
                    num.add(numTran);
                   
                   
                   numTran = Integer.parseInt(texto.substring(i+3,i+4));
                   grafo.eventoBegin(new Transaction(numTran));
                   //lm.adicionarTransacao(new Transaction(numTran));
                   System.out.println("-----------------------------------------------------------\n");
                   grafo.percorrerEstados();
                   System.out.println("\n-----------------------------------------------------------\n");
         
                   
                   i = i + 5;
                   break;
                   
               case "r":
                       
                   numTran = Integer.parseInt(texto.substring(i+1,i+2));
                   if(grafo.verificarTransacao(numTran)) {
                     
                     
                     if(lm.Lock_Table.isEmpty())
                         grafo.eventoReadWrite(numTran); 
                     
                    // if(dados.containsKey(texto.substring(i+4, i+5))) {
                      D = dados.get(texto.substring(i+3, i+4));
                     
                  
                     lm.adicionarTransacao(grafo.retornarTransacao(numTran));
                     grafo = lm.LS(grafo.retornarTransacao(numTran), D);
                     
                     System.out.println("-----------------------------------------------------------\n");
                     grafo.percorrerEstados();
                     System.out.println("\n-----------------------------------------------------------\n");
                     
                   } else {
                     System.out.println("Não existe transacao, no Read");
                     texto = menuErro();
                     grafo.apagarTransacoes();
                     i = 0;
                     break;
                   }
                   
                   i = i + 5;
                   break;
               case "w":
                  
                   numTran = Integer.parseInt(texto.substring(i+1,i+2));
                   if(grafo.verificarTransacao(numTran)) {
                     //grafo.eventoReadWrite(numTran); 
                     if(lm.Lock_Table.isEmpty())
                         grafo.eventoReadWrite(numTran);
                    
                     System.out.println("-----------------------------------------------------------\n");
                     grafo.percorrerEstados();
                     System.out.println("\n-----------------------------------------------------------\n");
                     
                     D = dados.get(texto.substring(i+3, i+4)); 
                     
                     lm.adicionarTransacao(grafo.retornarTransacao(numTran));
                     grafo = lm.LX(grafo.retornarTransacao(numTran), D);   
                     
                     System.out.println("-----------------------------------------------------------\n");
                     grafo.percorrerEstados();
                     System.out.println("\n-----------------------------------------------------------\n");
                     
                   } else {
                     System.out.println("Não existe transacao, no Write");
                     texto = menuErro();
                     grafo.apagarTransacoes();
                     i = 0;
                     break;
                   }
                   i = i + 5;
                   break;
               case "C":                  
                   
                   numTran = Integer.parseInt(texto.substring(i+2, i+3));
                   
                   if(grafo.verificarTransacao(numTran)) {
                       grafo.eventoTerminate(numTran);
                       grafo.eventoCommit(numTran); 
                     
                       /*
                       Transaction tr = grafo.retornarTransacao(numTran);
                       ArrayList<ItemDados> lb = tr.getElementosBloqueados();
                       for(ItemDados d: lb) {
                           if(lm.Wait_Q.contains(d)) {
                               
                           }
                                
                       } */
                       
                       grafo.transactionsOnGraph.get(numTran).removerBloqueios();
                       lm.removerTransacao(grafo.retornarTransacao(numTran));
                       
                      // grafo.transactionsOnGraph.get(numTran).adicionarBloqueio();
                       
                       
                        
                      
                       System.out.println("-----------------------------------------------------------\n");
                       grafo.percorrerEstados();
                       System.out.println("\n-----------------------------------------------------------\n");
                       
                       

                   }
                   i = i + 4;
                   break;
               default:
                   texto = menuErro();
                   i = 0;
                   break;
           }    
            
        }
         for(int  k = 0; k < num.size(); k++) {
            grafo.eventoFinisth(k);
        }
         
         System.out.println("-----------------------------------------------------------\n");
         grafo.percorrerEstados();
         System.out.println("\n-----------------------------------------------------------\n"); 
         
    }
    
    public static void menuHistorico() {
        
        Tr_Manager  grafo = new Tr_Manager();
        
        //ENTRADA DE DADOS
        Scanner historia = new Scanner(System.in);
        String texto ;
       
        
        //teste
         grafo.inicializarGrafo();
       
        menu2();
        texto = historia.nextLine();
        
        //VERIFICACAO DOS DADOS
        
        tratador(texto, grafo);
        

         
        
    }
}
