import java.util.ArrayList;

/**
 * 
 * Sempre que uma transação for criada, deve-se criar uma posição para a mesma em Lock_Table.
 * Sempre que um item de dados for criado, deve-se criar uma fila para o mesmo em Wait_Q.
 * Podemos usar o padrão Observer para lidar com isso, caso não seja desejável realizar as inserções explicitamente.
 * 
 * Assumimos que uma vez que uma transação pediu um bloqueio para um item e entra na fila de espera, não irá
 * pedir novamente o item até que o seu pedido seja atendido.
 * 
 */

public class Lock_Manager 
{
    Tr_Manager grafo;
     ArrayList<Fila<Bloqueio>> Wait_Q = new ArrayList<Fila<Bloqueio>>();
    ArrayList<Transaction> Lock_Table = new ArrayList<Transaction>();
    
    public static final int X = 1; // Bloqueio exclusivo. 
    public static final int S = 2; // Bloqueio compartilhado.
    public static final int LIVRE = 0; // Não há bloqueio para o elemento.
    
    // Construtores.
    
    public Lock_Manager()
    {
        // Construtor padrão usado somente para testes.
    }
    
    public Lock_Manager(Tr_Manager g) {
        grafo = g;
    }
    
    // Métodos para a Wait_Q.
    
    public void criarFilaItem(int itemID)
    {
        Wait_Q.add(itemID, new Fila());
    }
    
    public boolean adicionarFila(int itemID, Bloqueio b)
    {
        Fila temp = Wait_Q.get(itemID);
        //temp.add(b);
        //System.out.println(temp.elementos());
        return temp.add(b);
        
    }
    
    public Bloqueio removerFila(int itemID)
    {
        Fila temp = Wait_Q.get(itemID);
        return (Bloqueio) temp.remove();
    }
    
    public Fila<Bloqueio> getFila(int itemID)
    {
        return Wait_Q.get(itemID);
    }
    
    public void limparWaitQ()
    {
        Wait_Q.clear();
    }
    
    // Métodos para a Lock_Table.
    
    public boolean adicionarTransacao(Transaction tr)
    {
        return Lock_Table.add(tr);
    }
    
    public boolean removerTransacao(Transaction tr)
    {
        return Lock_Table.remove(tr);
    }
    
    public void limparLockTable()
    {
        Lock_Table.clear();
    }
    
    // Métodos que serão utilizados pelas funções básicas.
    
    public Transaction verificarTran(Transaction Tj, ItemDados D) {
        Transaction Ti = null;
        for(Transaction t: Lock_Table) {            
            if(t.getElementosBloqueados().contains(D)) {
                 Ti = t;
            }
        }
        return Ti;
    }
    
    public void concederBloqueio(Transaction Tr, ItemDados D, int tipoBloqueio)
    {
        Lock_Table.add(Tr.getID(), Tr);
        Tr.adicionarBloqueio(D);
        D.setBloqueio(tipoBloqueio);
    }
    
    public void evitarDeadlock(Transaction Ti, ItemDados D, int tipoBloqueio)          
    {
        Transaction Tj = verificarTran(Ti, D); // Descobrimos qual transação Tj possui o bloqueio.
     
        if(Tj != null)
        {
            WoundWait(Ti, Tj, D, tipoBloqueio);
        }
    }
    
    // Técnica de Wound-Wait.
    
    public void WoundWait(Transaction Ti, Transaction Tj, ItemDados D, int tipoBloqueio)
    {   
        System.out.println("(entrou)Ti Timestamp: "+Ti.getTS());
        System.out.println("(possui)Tj Timestamp: "+Tj.getTS());
        if(Ti.getTS() < Tj.getTS()) // Ti possui prioridade mais alta.
        {    
            grafo.eventoRollback(Tj.getID());
            Tj.removerBloqueios();
            
            System.out.println("-----------------------------------------------------------\n");
            grafo.percorrerEstados();
            System.out.println("\n-----------------------------------------------------------\n");
             
            grafo.eventoReadWrite(Tj.getID());
            concederBloqueio(Ti, D, tipoBloqueio);
            
        }
        else
        {
            adicionarFila(D.getID(), new Bloqueio(Ti.getID(), D.getID(), tipoBloqueio));
        }
    }
    
    // Funções básicas.
    
    /**
    * 
    * No caso de existir bloqueio, temos o que segue.
    * Se o bloqueio for compartilhado, devemos verificar se na fila existe algum bloqueio exclusivo.
    * Se existir, chamamos o evitarDeadlock. Senão, todos os bloqueios sobre o item são compartilhados
    * e podemos dar à transação Tr o bloqueio compartilhado sobre o item.
    * 
    */
    
    public Tr_Manager LS(Transaction Tr, ItemDados D)
    {
        if(D.livreBloqueio())
        {
            concederBloqueio(Tr, D, Lock_Manager.S);
        }
        else
        {
            if(D.bloqueioExclusivo())
            {
                evitarDeadlock(Tr, D, Lock_Manager.S);
            }
            if(D.bloqueioCompartilhado())
            {
                Fila temp = Wait_Q.get(D.getID()); // Transações que esperam pelo item D.
                boolean existeExclusivo = false; // Diz se existe ou não um bloqueio exclusivo para o item D.
                
                for(Object b: temp.elementos())
                {
                    Bloqueio aux = (Bloqueio) b;
                    if(aux.getTipo() == Lock_Manager.X)
                    {
                        existeExclusivo = true;
                        break;
                    }
                }
                
                if(existeExclusivo)
                {
                    evitarDeadlock(Tr, D, Lock_Manager.S);
                }
                else
                {
                    concederBloqueio(Tr, D, Lock_Manager.S);
                }
            }
        }
        
        return grafo;
    }
    
    public Tr_Manager LX(Transaction Tr, ItemDados D)
    {
        if(D.livreBloqueio())
        {
            concederBloqueio(Tr, D, Lock_Manager.X);
        }
        else
        {
            evitarDeadlock(Tr, D, Lock_Manager.X);
            
        }
        
        return grafo;
    }
    
    public boolean U(Transaction Tr, ItemDados D)
    {
        boolean verificar = false;
        
        if(Lock_Table.contains(Tr))
        {
            verificar = Tr.removerBloqueio(D);
            D.setBloqueio(Lock_Manager.LIVRE);
        }
        
        if(!verificar)
        {
            System.out.println("Transação inexistente ou o item dado não é bloqueado pela transação.");
        }
        
        return verificar;
    }
}