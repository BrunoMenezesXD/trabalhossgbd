import java.util.ArrayList;

public class Transaction 
{
    private int id;
    private int timestamp;
    private String estado = null;
    private ArrayList<ItemDados> elementosBloqueados = new ArrayList<ItemDados>();
    
    /**
    * Primeiro construtor refere-se a parte do segundo trabalho
    */
    public Transaction() {}
    /**
     * Segundo construtor usa na classe User, na parte da historia
     * Recebe apenaso identificador da transacao, sem passar o timestamp
     * @param identificador
     */
    public Transaction(int identificador) {
        id = identificador;
    }
    /**
     * Construtor passando dois parametros o identificador da transacao e o timestamp
     * @param identificador
     * @param ts 
     */
    public Transaction(int identificador, int ts)
    {
    	id = identificador;
        timestamp = ts;
    }
    /**
     * Recebe o estado atual da transacao 
     * @param e 
     */
    public void setEstado(String e)
    {
        estado = e;
    }
    /**
     * Retorna o estado atual da transacao
     * @return estado 
     */
    public String getEstado()
    {
        return estado;
    }
    /**
     * Retorna o id da transacao
     * @return id 
     */
    public int getID()
    {
    	return id;
    }
    /**
     * Adiciona o timestamp da transacao
     * @param ts 
     */
    public void setTs(int ts) {
        timestamp = ts;
    }
    /**
     * Retorna o valor do timestamp na transacao
     * @return timestamp 
     */
    public int getTS()
    {
        return timestamp;
    }
    
    public ArrayList<ItemDados> getElementosBloqueados()
    {
        return elementosBloqueados;
    }
    
    public boolean adicionarBloqueio(ItemDados item)
    {
   	return elementosBloqueados.add(item);
    }
   	
   public boolean removerBloqueio(ItemDados item)
   {
       item.setBloqueio(Lock_Manager.LIVRE);
       return elementosBloqueados.remove(item);
   }
   
   public void removerBloqueios()
   {   
       for(ItemDados aux: elementosBloqueados)
       {
            aux.setBloqueio(Lock_Manager.LIVRE); // Chamar removerBloqueio(aux) causa uma exceção.
       }
       
       elementosBloqueados.clear();
   }
   
   public void imprimirElementosBloqueados()
   {
       if(elementosBloqueados.isEmpty())
       {
           System.out.println("A transação não possui bloqueios.");
           return;
       }
        
       for(ItemDados aux: elementosBloqueados)
       {
           System.out.println("Elemento bloqueado: "+aux.getID()+"."+"Tipo de bloqueio: "+aux.getTipoBloqueio()+".");
       }
   }
}