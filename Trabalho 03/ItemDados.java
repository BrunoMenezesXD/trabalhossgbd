import java.util.ArrayList;

/**
 *  Esta classe é utilizada para "marcar" um objeto como sendo um item de dados.
 * 
 */

public class ItemDados
{
    private Object obj;
    private int id;
    private int tipoBloqueio;
    
    public ItemDados(Object item, int identificador)
    {
        obj = item;
        id = identificador;
        tipoBloqueio = Lock_Manager.LIVRE;
    }
    
    public void setItem(Object o)
    {
        obj = o;
    }
    
    public void setBloqueio(int tipo)
    {
        tipoBloqueio = tipo;
    }
    
    public Object getItem()
    {
        return obj;
    }
    
    public int getID()
    {
        return id;
    }
    
    public int getTipoBloqueio()
    {
        return tipoBloqueio;
    }
    
    public boolean livreBloqueio()
    {
        return tipoBloqueio == Lock_Manager.LIVRE;
    }
    
    public boolean bloqueioExclusivo()
    {
        return tipoBloqueio == Lock_Manager.X;
    }
    
    public boolean bloqueioCompartilhado()
    {
        return tipoBloqueio == Lock_Manager.S;
    }
}
