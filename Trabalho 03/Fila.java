import java.util.ArrayList;

/**
 * Fila parametrizada.
 * 
 * Implementamos a fila utilizando um ArrayList.
 * 
 * @param <T>
 */

public class Fila<T> 
{
    private ArrayList<T> fila = new ArrayList<T>();
    
    public boolean add(T s)
    {
        return fila.add(s);
    }
    
    public T remove()
    {
        return fila.remove(0);
    }
    
    public boolean isEmpty()
    {
        return fila.isEmpty();
    }
    
    public int size()
    {
        return fila.size();
    }
    
    public ArrayList<T> elementos()
    {
        return fila;
    }
}