/**
 *
 * 
 */

public class Bloqueio 
{
    int idTr, idItem, tipo;
    
    public Bloqueio(int tr, int item, int t)
    {
        idTr = tr;
        idItem = item;
        tipo = t;
    }
    
    public int getTr()
    {
        return idTr;
    }
    
    public int getItem()
    {
        return idItem;
    }
    
    public int getTipo()
    {
        return tipo;
    }
}